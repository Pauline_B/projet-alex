import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {GlobalEventsManagerService} from '../services/global-events-manager.service';

@Component({
  selector: 'app-suivi',
  templateUrl: './suivi.component.html',
  styleUrls: ['./suivi.component.css']
})
export class SuiviComponent implements OnInit {

  constructor(private router: Router,
              private globalEventsManagerService: GlobalEventsManagerService) { }

  ngOnInit() {
    this.onLoginSuccessfully();
  }

  onLoginSuccessfully() {
    this.globalEventsManagerService.showNavBarFunction(true);
  }

}
