import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {GlobalEventsManagerService} from '../services/global-events-manager.service';

@Component({
  selector: 'app-choice',
  templateUrl: './choice.component.html',
  styleUrls: ['./choice.component.css']
})
export class ChoiceComponent implements OnInit {

  constructor(private router: Router,
              private globalEventsManagerService: GlobalEventsManagerService) { }

  ngOnInit() {
    this.globalEventsManagerService.showNavBarFunction(false);
  }
}
