import { Component, OnInit } from '@angular/core';
import {GlobalEventsManagerService} from '../services/global-events-manager.service';
import {Router} from '@angular/router';
import {ModalService} from '../services/modal.service';

@Component({
  selector: 'app-anamnese',
  templateUrl: './anamnese.component.html',
  styleUrls: ['./anamnese.component.css']
})
export class AnamneseComponent implements OnInit {

  constructor(private modalService: ModalService,
              private router: Router,
              private globalEventsManagerService: GlobalEventsManagerService) { }

  ngOnInit() {
    this.globalEventsManagerService.showNavBarFunction(true);
  }

  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }

}
