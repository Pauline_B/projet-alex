import {Component, OnInit} from '@angular/core';
import {GlobalEventsManagerService} from '../services/global-events-manager.service';
import {AuthService} from '../services/auth.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  showNavBar = false;
  authStatus = true;

  constructor(private globalEventsManagerService: GlobalEventsManagerService,
              private authService: AuthService) {
    this.globalEventsManagerService.showNavBarEmitter.subscribe((mode) => {
      this.showNavBar = mode;
    });
  }

  ngOnInit() {
    this.authStatus = this.authService.isAuth;
  }

  onSignOut() {
    this.authService.signOut();
    this.authStatus = this.authService.isAuth;
    this.globalEventsManagerService.showNavBarFunction(false);
  }

}
