import {Component, OnInit} from '@angular/core';
import {ModalService} from '../services/modal.service';
import {Router} from '@angular/router';
import {GlobalEventsManagerService} from '../services/global-events-manager.service';

@Component({
  selector: 'app-infos',
  templateUrl: './infos.component.html',
  styleUrls: ['./infos.component.css']
})
export class InfosComponent implements OnInit {

  constructor(private modalService: ModalService,
              private router: Router,
              private globalEventsManagerService: GlobalEventsManagerService) {
  }

  // isVisible = false;

  ngOnInit() {
    this.globalEventsManagerService.showNavBarFunction(true);
  }

  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }
}
