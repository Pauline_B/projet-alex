import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {GlobalEventsManagerService} from '../services/global-events-manager.service';

@Component({
  selector: 'app-grossesse',
  templateUrl: './grossesse.component.html',
  styleUrls: ['./grossesse.component.css']
})
export class GrossesseComponent implements OnInit {

  constructor(private router: Router,
              private globalEventsManagerService: GlobalEventsManagerService) { }

  ngOnInit() {
    this.globalEventsManagerService.showNavBarFunction(true);
  }

}
