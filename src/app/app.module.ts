import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { ChoiceComponent } from './choice/choice.component';
import { InfosComponent } from './infos/infos.component';
import { GrossesseComponent } from './grossesse/grossesse.component';
import { SuiviComponent } from './suivi/suivi.component';
import { AnamneseComponent } from './anamnese/anamnese.component';
import { AuthComponent } from './auth/auth.component';
import { ConstructComponent } from './construct/construct.component';
import { ModalEtatCivilComponent } from './modal-etat-civil/modal-etat-civil.component';
import { ModalComponent } from './modal/modal.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';

import { AuthService } from './services/auth.service';
import { ModalService} from './services/modal.service';
import {GlobalEventsManagerService} from './services/global-events-manager.service';
import {Routes} from '@angular/router';

const routes: Routes = [
  { path: 'auth', component: AuthComponent},
  { path: 'choice', component: ChoiceComponent},
  { path: 'infos', component: InfosComponent},
  { path: 'etat-civil', component: ModalEtatCivilComponent},
  { path: 'grossesse', component: GrossesseComponent},
  { path: 'suivi', component: SuiviComponent},
  { path: 'anamnese', component: AnamneseComponent},
  { path: 'menu', component: NavBarComponent},
  { path: 'construct', component: ConstructComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    ChoiceComponent,
    InfosComponent,
    GrossesseComponent,
    SuiviComponent,
    AnamneseComponent,
    AuthComponent,
    ConstructComponent,
    ModalEtatCivilComponent,
    ModalComponent,
    NavBarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    AuthService,
    ModalService,
    GlobalEventsManagerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
