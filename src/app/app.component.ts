import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {GlobalEventsManagerService} from './services/global-events-manager.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private router: Router,
              private globalEventsManagerService: GlobalEventsManagerService) {}

  ngOnInit() {
    this.router.navigate(['auth']);
    this.globalEventsManagerService.showNavBarFunction(false);
  }
}
