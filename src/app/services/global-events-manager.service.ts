import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable()
export class GlobalEventsManagerService {

  private showNavBar: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public showNavBarEmitter: Observable<boolean> = this.showNavBar.asObservable();

  constructor() {}

  showNavBarFunction(ifShow: boolean) {
    this.showNavBar.next(ifShow);
  }

}
